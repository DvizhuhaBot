#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import logging
import getpass
import json
import io
import os
from optparse import OptionParser

import sleekxmpp

if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding('utf8')
else:
    raw_input = input

class MUCBot(sleekxmpp.ClientXMPP):
    def __init__(self, jid, password, room, nick, rpass):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)

        self.room  = room
        self.nick  = nick
        self.rpass = rpass

        self.users = {}

        self.add_event_handler("session_start", self.start)
        #self.add_event_handler("groupchat_message", self.muc_message)
        self.add_event_handler('message', self.message)
        
    def start(self, event):
        self.get_roster()
        self.send_presence()
        
        # Check for db.json, if empty or not there, create it.
        if (not os.path.isfile("db.json") or os.path.getsize("db.json") == 0):
            print("WARNING\tEvent database not found or empty, creating new one.")
            with io.open("db.json", "w", encoding="utf8") as f:
                f.write("[]")

        if (not os.path.isfile("users.json") or os.path.getsize("users.json") == 0):
            print("WARNING\tUser database not found or empty, creating new one.")
            with io.open("users.json", "w", encoding="utf8") as f:
                f.write("[]")

        with open("users.json") as f:
            self.users = json.load(f)

        if (self.rpass):
            self.plugin['xep_0045'].joinMUC(self.room,
                                            self.nick,
                                            password=self.rpass,
                                            wait=True)
        else:
            self.plugin['xep_0045'].joinMUC(self.room,
                                            self.nick,
                                            wait=True)            

    def message(self, msg):
        if ((msg['type'] in ('normal', 'chat'))
            and (msg['from'].bare in self.users)
            and (msg['mucnick'] != self.nick)):

            whos = msg['from'].bare
            
            if "!ping" in msg['body']:
                msg.reply("Pong!").send()
                
            if "!add" in msg['body']:
                addargs = msg['body'].split('"')[1::2]

                if (len(addargs) != 2):
                    msg.reply('Юзаж: !add "дата" "событие", ковычки обязательны!').send()
                else:
                    with io.open("db.json", encoding="utf8") as f:
                        data = json.load(f)
                        
                    add_dict = {
                        'date'  : addargs[0],
                        'event' : addargs[1],
                        'by'    : whos
                    }
                    
                    data.append(add_dict)
                    
                    with io.open('db.json', 'w', encoding="utf8") as f:
                        f.write(json.dumps(data, sort_keys=True, indent=4, ensure_ascii=False))

                    msg.reply("Движуха добавлена.").send()
                    self.send_message(mto = self.room,
                                      mbody = "{} добавил движуху: {}".format(whos, addargs[0]),
                                      mtype = 'groupchat')
                            
            if "!del" in msg['body']:
                delargs = msg['body'].split('"')[1::2]

                if (len(delargs) != 1):
                    msg.reply('Юзаж: !del "дата", ковычки обязательны!').send()
                    
                else:
                    deleted = False
                    with io.open("db.json", encoding="utf8") as f:
                        data = json.load(f)

                    for i in range(len(data)):
                        if data[i]["date"] == delargs[0]:
                            data.pop(i)
                            deleted = True
                            break

                    if (deleted):
                        with io.open('db.json', 'w', encoding="utf8") as f:
                            f.write(json.dumps(data, sort_keys=True, indent=4, ensure_ascii=False))
                            msg.reply("Движуха удалена.").send()
                            self.send_message(mto = self.room,
                                              mbody = "{} удалил движуху: {}".format(whos, delargs[0]),
                                              mtype = 'groupchat')
                    else:
                        msg.reply("Движуха {} не найдена.".format(delargs[0])).send()
                    

            if "!list" in msg['body']:
                with open("db.json") as f:
                    data = json.load(f)

                if (data == []):
                    msg.reply("В настоящий момент, движух нет.").send()
                else:
                    result =  "Текущий список движух:\n"
                    result += ("-" * 60) + "\n"
                    result += "{:<20} {:<20}\n".format("Дата", "Движуха")
                    result += ("-" * 60) + "\n"
                    
                    for i in range(len(data)):
                        result += "{:<20} {:<20}\n".format(data[i]["date"], data[i]["event"])
                        result += ("-" * 60) + "\n"

                    msg.reply(result).send()
                        
            if "!help" in msg['body']:
                helpstr = (
                '\nДвижуха v0.3 "Почти Гамма"\n'
                'Комманды:\n'
                '!ping\n'
                '    Очевидно же.\n\n'
                '!add "дата" "событие"\n'
                '    Добавить новую движуху.\n\n'
                '!del "дата"\n'
                '    Удалить движуху.\n\n'
                '!list\n'
                'Список текущих движух.\n'
                )

                msg.reply(helpstr).send()
                

if __name__ == '__main__':
    # Setup the command line arguments.
    optp = OptionParser()

    # Output verbosity options.
    optp.add_option('-q', '--quiet', help='set logging to ERROR',
                    action='store_const', dest='loglevel',
                    const=logging.ERROR, default=logging.INFO)
    optp.add_option('-d', '--debug', help='set logging to DEBUG',
                    action='store_const', dest='loglevel',
                    const=logging.DEBUG, default=logging.INFO)
    optp.add_option('-v', '--verbose', help='set logging to COMM',
                    action='store_const', dest='loglevel',
                    const=5, default=logging.INFO)

    # JID and password options.
    optp.add_option("-j", "--jid", dest="jid",
                    help="JID to use")
    optp.add_option("-s", "--rpass", dest="rpass",
                    help="Room password, if any.")
    optp.add_option("-p", "--pass", dest="password",
                    help="password to use")
    optp.add_option("-r", "--room", dest="room",
                    help="MUC room to join")
    optp.add_option("-n", "--nick", dest="nick",
                    help="MUC nickname")

    opts, args = optp.parse_args()

    # Setup logging.
    logging.basicConfig(level=opts.loglevel,
                        format='%(levelname)-8s %(message)s')

    if opts.jid is None:
        opts.jid = raw_input("Username: ")
    if opts.password is None:
        opts.password = getpass.getpass("Password: ")
    if opts.room is None:
        opts.room = raw_input("MUC room: ")
    if opts.nick is None:
        opts.nick = raw_input("MUC nickname: ")

    # Setup the MUCBot and register plugins. Note that while plugins may
    # have interdependencies, the order in which you register them does
    # not matter.
    xmpp = MUCBot(opts.jid, opts.password, opts.room, opts.nick, opts.rpass)
    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0045') # Multi-User Chat
    xmpp.register_plugin('xep_0199') # XMPP Ping

    # Connect to the XMPP server and start processing XMPP stanzas.
    if xmpp.connect():
        # If you do not have the dnspython library installed, you will need
        # to manually specify the name of the server if it does not match
        # the one in the JID. For example, to use Google Talk you would
        # need to use:
        #
        # if xmpp.connect(('talk.google.com', 5222)):
        #     ...
        xmpp.process(block=True)
        print("Done")
    else:
        print("Unable to connect.")
        
